/********************************************************************************
** Form generated from reading UI file 'planedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLANEDIALOG_H
#define UI_PLANEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PlaneDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QDoubleSpinBox *spinXstart;
    QDoubleSpinBox *spinXend;
    QDoubleSpinBox *spinYstart;
    QDoubleSpinBox *spinYend;
    QSpinBox *spinXres;
    QSpinBox *spinYres;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_2;
    QPushButton *pushButton;

    void setupUi(QDialog *PlaneDialog)
    {
        if (PlaneDialog->objectName().isEmpty())
            PlaneDialog->setObjectName(QStringLiteral("PlaneDialog"));
        PlaneDialog->resize(290, 230);
        verticalLayout = new QVBoxLayout(PlaneDialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(PlaneDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(PlaneDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(PlaneDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(PlaneDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(PlaneDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(PlaneDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        spinXstart = new QDoubleSpinBox(PlaneDialog);
        spinXstart->setObjectName(QStringLiteral("spinXstart"));
        spinXstart->setDecimals(6);
        spinXstart->setMinimum(-999999);
        spinXstart->setMaximum(1e+6);

        formLayout->setWidget(0, QFormLayout::FieldRole, spinXstart);

        spinXend = new QDoubleSpinBox(PlaneDialog);
        spinXend->setObjectName(QStringLiteral("spinXend"));
        spinXend->setDecimals(6);
        spinXend->setMinimum(-999999);
        spinXend->setMaximum(1e+6);
        spinXend->setValue(10);

        formLayout->setWidget(1, QFormLayout::FieldRole, spinXend);

        spinYstart = new QDoubleSpinBox(PlaneDialog);
        spinYstart->setObjectName(QStringLiteral("spinYstart"));
        spinYstart->setDecimals(6);
        spinYstart->setMinimum(-999999);
        spinYstart->setMaximum(1e+6);

        formLayout->setWidget(2, QFormLayout::FieldRole, spinYstart);

        spinYend = new QDoubleSpinBox(PlaneDialog);
        spinYend->setObjectName(QStringLiteral("spinYend"));
        spinYend->setDecimals(6);
        spinYend->setMinimum(-999999);
        spinYend->setMaximum(1e+6);
        spinYend->setValue(10);

        formLayout->setWidget(3, QFormLayout::FieldRole, spinYend);

        spinXres = new QSpinBox(PlaneDialog);
        spinXres->setObjectName(QStringLiteral("spinXres"));
        spinXres->setMinimum(10);
        spinXres->setMaximum(3000);

        formLayout->setWidget(4, QFormLayout::FieldRole, spinXres);

        spinYres = new QSpinBox(PlaneDialog);
        spinYres->setObjectName(QStringLiteral("spinYres"));
        spinYres->setMinimum(10);
        spinYres->setMaximum(3000);

        formLayout->setWidget(5, QFormLayout::FieldRole, spinYres);


        verticalLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_2 = new QPushButton(PlaneDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton = new QPushButton(PlaneDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(PlaneDialog);
        QObject::connect(pushButton_2, SIGNAL(clicked()), PlaneDialog, SLOT(accept()));
        QObject::connect(pushButton, SIGNAL(clicked()), PlaneDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PlaneDialog);
    } // setupUi

    void retranslateUi(QDialog *PlaneDialog)
    {
        PlaneDialog->setWindowTitle(QApplication::translate("PlaneDialog", "PlaneDialog", 0));
        label->setText(QApplication::translate("PlaneDialog", "x start", 0));
        label_2->setText(QApplication::translate("PlaneDialog", "x end", 0));
        label_3->setText(QApplication::translate("PlaneDialog", "y start", 0));
        label_4->setText(QApplication::translate("PlaneDialog", "y end", 0));
        label_5->setText(QApplication::translate("PlaneDialog", "x resolution", 0));
        label_6->setText(QApplication::translate("PlaneDialog", "y resolution", 0));
        pushButton_2->setText(QApplication::translate("PlaneDialog", "Ok", 0));
        pushButton->setText(QApplication::translate("PlaneDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class PlaneDialog: public Ui_PlaneDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLANEDIALOG_H
