/********************************************************************************
** Form generated from reading UI file 'spheredialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPHEREDIALOG_H
#define UI_SPHEREDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SphereDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QDoubleSpinBox *radSpin;
    QSpinBox *thetaSpin;
    QSpinBox *phiSpin;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *SphereDialog)
    {
        if (SphereDialog->objectName().isEmpty())
            SphereDialog->setObjectName(QStringLiteral("SphereDialog"));
        SphereDialog->resize(270, 172);
        verticalLayout = new QVBoxLayout(SphereDialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(SphereDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(SphereDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(SphereDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        radSpin = new QDoubleSpinBox(SphereDialog);
        radSpin->setObjectName(QStringLiteral("radSpin"));
        radSpin->setMinimum(0.5);
        radSpin->setMaximum(100000);
        radSpin->setValue(5);

        formLayout->setWidget(0, QFormLayout::FieldRole, radSpin);

        thetaSpin = new QSpinBox(SphereDialog);
        thetaSpin->setObjectName(QStringLiteral("thetaSpin"));
        thetaSpin->setMinimum(2);
        thetaSpin->setMaximum(3000);
        thetaSpin->setValue(10);

        formLayout->setWidget(1, QFormLayout::FieldRole, thetaSpin);

        phiSpin = new QSpinBox(SphereDialog);
        phiSpin->setObjectName(QStringLiteral("phiSpin"));
        phiSpin->setMinimum(2);
        phiSpin->setMaximum(3000);
        phiSpin->setValue(10);

        formLayout->setWidget(2, QFormLayout::FieldRole, phiSpin);


        verticalLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(SphereDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(SphereDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(SphereDialog);
        QObject::connect(pushButton, SIGNAL(clicked()), SphereDialog, SLOT(accept()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), SphereDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SphereDialog);
    } // setupUi

    void retranslateUi(QDialog *SphereDialog)
    {
        SphereDialog->setWindowTitle(QApplication::translate("SphereDialog", "SphereDialog", 0));
        label->setText(QApplication::translate("SphereDialog", "Sphere radius", 0));
        label_2->setText(QApplication::translate("SphereDialog", "Theta resolution", 0));
        label_3->setText(QApplication::translate("SphereDialog", "Phi resolution", 0));
        pushButton->setText(QApplication::translate("SphereDialog", "Ok", 0));
        pushButton_2->setText(QApplication::translate("SphereDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class SphereDialog: public Ui_SphereDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPHEREDIALOG_H
